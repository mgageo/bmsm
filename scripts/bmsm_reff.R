# <!-- coding: utf-8 -->
#
# les traitements sur les données
#
#
# les données rousserelle effarvate
# source("geo/scripts/bmsm.R"); reff_jour()
reff_jour <- function() {
  reff_histo()
  reff_atlas()
}
# source("geo/scripts/bmsm.R"); reff_histo()
reff_histo <- function() {
  library(readxl)
  library(sf)
  library(tidyverse)
  dsn <- sprintf("%s/reff/r ff.xlsx", varDir)
  df <- readxl::read_excel(dsn, col_names = TRUE)
  df <- df[, 1:29]
  colnames(df) <- c('code', 'espece', 'groupe', 'dept', 'commune', 'lieudit', 'milieu', 'coordonnees', 'periode10', 'periode5', 'activite', 'date', 'annee', 'mois', 'remarques_brutes', 'remarques', 'code_nicheur', 'rem3', 'auteur', 'auteur_code', 'auteur_ajout', 'carte', 'cp', 'nombre', 'nombre_mbe', 'en', 'appartient', 'transmission', 'protocole')
  glimpse(df)
  df$X <- gsub(';.*$', '', df$coordonnees)
  df$Y <- gsub('^.*;', '', df$coordonnees)
  cols.num <- c("X", "Y")
  df[cols.num] <- sapply(df[cols.num], as.numeric)
  head(df)
  df <- df %>%
    filter(!is.na(X))
  glimpse(df)
  nc <- st_as_sf(df, coords = c("X", "Y"), crs = 2154)
  opar <- par(mar = c(0,0,0,0))
  grille.sf <- fonds_grille_lire_sf()
  plot(st_geometry(grille.sf), bg='lightblue', col='white')
  couleur <- misc_col('red', 60)
  plot(st_geometry(nc), col=couleur, pch=19, cex=3, add=TRUE)
  st_crs(grille.sf) <- 2154
  nc1 <- nc %>%
    st_join(grille.sf) %>%
    glimpse()
  df <- nc1 %>%
    st_drop_geometry() %>%
    group_by(EN) %>%
    summarize(nb=n()) %>%
    glimpse()
}
# source("geo/scripts/bmsm.R"); reff_atlas()
reff_atlas <- function() {
  library(sf)
  library(tidyverse)
  nc <- donnees_lire_sf()
  nc <- nc %>%
    filter(TAXO_VERNA == "Rousserolle effarvatte")
  st_crs(nc) <- 2154
  glimpse(nc)
  opar <- par(mar = c(0,0,0,0))
  grille.sf <- fonds_grille_lire_sf() %>%
    glimpse()

  plot(st_geometry(grille.sf), bg='lightblue', col='white')
  couleur <- misc_col('red', 60)
  plot(st_geometry(nc), col=couleur, pch=19, cex=3, add=TRUE)
  st_crs(grille.sf) <- 2154
  nc1 <- nc %>%
    st_join(grille.sf) %>%
    glimpse()
  df <- nc1 %>%
    st_drop_geometry() %>%
    group_by(EN) %>%
    summarize(nb=n()) %>%
    glimpse()
}
