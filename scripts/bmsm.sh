#!/bin/sh
# <!-- coding: utf-8 -->
#T BMSM
# auteur: Marc Gauthier
  while read f; do
    [ -f $f ] || die "$0 $f"
    . $f
  done <<EOF
../win32/scripts/misc.sh
../geo/scripts/bmsm_sqlite.sh
EOF
#f CONF:
CONF() {
  LOG "CONF debut"
#  _ENV_bmsm_sqlite
  CFG="BMSM"
  [ -d "${CFG}" ] || mkdir "${CFG}"
  LOG "CONF fin"
}
#f _ENV_bmsm_sqlite: l'environnement spatialite
_ENV_bmsm_sqlite() {
 [ -f ../win32/scripts/misc_sqlite.sh ] || die "_ENV_keolis_sqlite() misc_sqlite"
  _ENV_sqlite
  . ../win32/scripts/misc_sqlite.sh
  db="bmsm"
  Base=${CFG}/${db}.sqlite
  Base=${db}.sqlite
}
#F e: edition des principaux fichiers
e() {
  LOG "e debut"
  for f in scripts/bmsm*.sh ; do
    E $f
  done
  for f in scripts/bmsm*.R ; do
    E $f
  done
  ( cd /d/bvi35/Couches${CFG}; explorer . &)
  ( cd ${CFG}; explorer . &)
  LOG "e fin"
}
wm() {
  WM d:/web/bv d:/web.heb/bv &
}
TOMTOM() {
  _ENV_gpsbabel
  gpsbabel -i kml -f ${CFG}/secteur5.kml -o TOMTOM -F ${CFG}/secteur5.ov2
}
#F GIT: pour mettre à jour le dépot git
GIT() {
  LOG "GIT debut"
  Local="${DRIVE}/web/geo";  Depot=bmsm; Remote=frama
  export Local
  export Depot
  export Remote
  export GIT_TRACING=2
  export GIT_CURL_VERBOSE=1
  _git_lst
  bash ../win32/scripts/git.sh INIT $*
#  bash ../win32/scripts/git.sh PUSH
  LOG "GIT fin"
}
#f _git_lst: la liste des fichiers pour le dépot
_git_lst() {
  cat  <<'EOF' > /tmp/git.lst
LICENCE.txt
scripts/bmsm.sh
EOF
  _ENV_R
  ( cd ..; R --vanilla -e "source('geo/scripts/misc_code.R');code_git('geo/scripts/bmsm.R')" )
  ls -1 BMSM/*.tex >> /tmp/git.lst
  cat  <<'EOF' >> /tmp/git.lst
LICENCE.txt
scripts/bmsm.sh
EOF
  cat  <<'EOF' > /tmp/README.md
# bmsm : atlas bmsm

Scripts en environnement Windows 10 : MinGW R MikTex

Ces scripts exploitent des données en provenance des bases Serena et Biolovision.

## Scripts R
Ces scripts sont dans le dossier "scripts".

## Tex
Le langage Tex est utilisé pour la production des documents.

MikTex est l'environnement utilisé.

Les fichiers .tex sont dans le dossier BMSM.

EOF
}
[ $# -eq 0 ] && ( HELP )
CONF
while [ "$1" != "" ]; do
  case $1 in
    -c | --conf )
      shift
      Conf=$1
      ;;
    * )
      $*
      exit 1
  esac
  shift
done