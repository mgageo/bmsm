# <!-- coding: utf-8 -->
#
# la partie occupation du sol
# ===============================================================
#
# source("geo/scripts/bmsm.R"); ocs_cesbio_emprise()
ocs_cesbio_emprise <- function(test=1) {
  carp()
  library(sf)
  library(tidyverse)
  extent.sf <- couches_extent_sf();
  grille.sf <- fonds_grille_lire_sf() %>%
    st_transform(2154)
  perimetre.sf <- couches_perimetre_lire_sf() %>%
    st_transform(2154) %>%
    glimpse()
  if ( ! exists('oso35.sf') ) {
    dsn <- sprintf("%s/departement_35.shp", osoDir)
    oso35.sf <<- st_read(dsn) %>%
      dplyr::select(Classe) %>%
      st_transform(2154) %>%
      glimpse()
  }
  if ( ! exists('oso50.sf') ) {
    dsn <- sprintf("%s/departement_50.shp", osoDir)
    oso50.sf <<- st_read(dsn) %>%
      dplyr::select(Classe) %>%
      st_transform(2154) %>%
      glimpse()
  }
  carp('intersection 35')
  nc <- oso35.sf
  nc35 <- st_intersection(nc, perimetre.sf) %>%
    dplyr::filter(Classe %in% c(41, 42)) %>%
    glimpse()
  nc <- oso50.sf
  nc50 <- st_intersection(nc, perimetre.sf) %>%
    dplyr::filter(Classe %in% c(41, 42)) %>%
    glimpse()
  dev.new()
  par(mfrow=c(1,1), mar=c(0,0,0,0), oma=c(0,0,0,0))
  plot(st_geometry(extent.sf), border = NA, col = NA, bg = "white")
  plot(st_geometry(nc35), add=TRUE)
  plot(st_geometry(nc50), add=TRUE)
}
# source("geo/scripts/bmsm.R");ocs_zones_mbe_ogc()
ocs_zones_mbe_ogc <- function(force=FALSE, test=1) {
  carp()
  library(tidyverse)
  library(sf)
  grille.sf <- fonds_grille_lire_sf() %>%
    glimpse()
  df <- zones_mbe_lire(FALSE) %>%
    filter(grepl('Dol', zone)) %>%
    glimpse()
  ogcDir <<- sprintf("%s/ogc", varDir)
  cfg_dir <<- ogcDir
  dir.create(ogcDir, showWarnings = FALSE)
  fonds.df <- read.table(text="fonction|fond|param|marge|size_max
read|rpg2010|%s/ogc/%s_rpg2010.json|300|2048
read|rpg2011|%s/ogc/%s_rpg2011.json|300|2048
read|rpg2012|%s/ogc/%s_rpg2012.json|300|2048
read|rpg2013|%s/ogc/%s_rpg2013.json|300|2048
read|rpg2014|%s/ogc/%s_rpg2014.json|300|2048
read|rpg2015|%s/ogc/%s_rpg2015.json|300|2048
read|rpg2016|%s/ogc/%s_rpg2016.json|300|2048
read|rpg2017|%s/ogc/%s_rpg2017.json|300|2048
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="") %>%
    filter(!grepl('^#', fonction)) %>%
    glimpse()
  for(i in 1:nrow(df)) {
#    ocs_zone_mbe_ogc(df, i, grille.sf, fonds.df, test=test)
    p <- df[i, 'zone']
    rpg.list <- list()
    for(j in 1:nrow(fonds.df)) {
      dsn <- sprintf(fonds.df[j, 'param'], varDir, p, fonds.df[j, 'fond'])
      carp('dsn: %s', dsn)
      a <- fonds.df[j, 'fond']
      a <- gsub('rpg', '', a)
      rpg.list[[a]] <- st_read(dsn, stringsAsFactors=FALSE, crs=2154) %>%
        mutate(annee=a) %>%
        glimpse()
    }
    dsn <- sprintf("%s/ocs_zones_mbe_%s.Rds", varDir, p)
    saveRDS(rpg.list, dsn)
    carp('dsn: %s', dsn)
#    break
  }
#  glimpse(rpg.list)
}
# source("geo/scripts/bmsm.R");ocs_zone_stat()
ocs_zone_stat <- function(p='Dol', force=FALSE, test=1) {
  carp()
  library(tidyverse)
  library(sf)
  library(janitor)
  dsn <- sprintf("%s/ocs_zones_mbe_%s.Rds", varDir, p)
  carp('dsn: %s', dsn)
  rpg.list <- readRDS(dsn)
  stat.list <- lapply(rpg.list, function(x) rpg_stat(x)) %>%
    glimpse()
# concaténation des dataframes
  df <- do.call("rbind", stat.list)
  rpg1_correspondance.df <- rpg1_correspondance_lire() %>%
    glimpse()
  df %>%
    ungroup() %>%
    glimpse() %>%
    left_join(rpg1_correspondance.df, by=c('CODE_GROUP'='code_groupe_culture')) %>%
    dplyr::select(code=CODE_GROUP,libelle=libelle_groupe_culture, annee, surface) %>%
    glimpse() %>%
    spread(annee, surface, fill=0) %>%
    adorn_totals(c("row")) %>%
    adorn_rounding(digits = 1, rounding = "half up") %>%
    print(n=30) %>%
    glimpse()
}
ocs_zone_mbe_ogc <- function(df, i, nc, fonds.df, test=1) {
  carp()
  library(tidyverse)
  library(sf)
  zone.sf <- nc %>%
    filter(EN == df[i, 'maille1'] | EN == df[i, 'maille2']) %>%
    glimpse()
  bbox.sf <- st_bbox(zone.sf) %>%
    glimpse()
  fonds_ogc_fonds(fonds.df, bbox.sf, df[i, 'zone'], test)
}