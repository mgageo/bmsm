# <!-- coding: utf-8 -->
#
# la partie Registre Parcellaire Graphique
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
#
# ==================================================================================================
#
#
# http://www.geocatalogue.fr/Detail.do?id=300875
# http://www.statistiques.developpement-durable.gouv.fr/donnees-ligne/th/territoires-1.html
# http://www.statistiques.developpement-durable.gouv.fr/fileadmin/documents/Produits_editoriaux/Donnees_en_ligne/Environnement/rpg_guide_d-utilisation_02.pdf
# https://www.geomayenne.fr/arcgis/rest/services/D53_SIG_REF/MapServer/legend
#
# http://giraudoux.pagesperso-orange.fr/#pgirbric
#
rpgDir <<- sprintf("%s/bvi35/CouchesRPG", Drive);
# les librairies
rpg_lib <- function() {
  library(rgdal)
}

rpg_wfs_dl <- function() {
  url <- 'wfs:https://marc:gauthier@wxs.ign.fr/1j199p5d9r9vswce59oe5tqk/geoportail/wfs?service=WFS&version=2.0.0'
  typename <- 'TYPENAMES=RPG.2012:rpg_2012'
  request <- 'REQUEST=DescribeFeatureType'
  source <- sprintf('%s&%s&%s', url, request, typename)
  sf::read_sf(source)
  sf::gdal_utils(util='info', source=source)
  sf::gdal_utils(util='vectortranslate', source='rgdal.geojson', destination='rgdal.json', options= c('-t_srs','EPSG:2154'))
}
rpg_dl <- function() {
  rpg_lib()
  dl_dir <<- sprintf("%s/rpg", target_dir)
  dir.create(dl_dir, showWarnings = FALSE, recursive = TRUE)
# http://www.statistiques.developpement-durable.gouv.fr/rpg/fichiers/telecharger?fichier=regional/SHP/rpg90_RBRE_RGF_SHP.zip
  les_url <- read.table(text="url|libelle
https://inspire.data.gouv.fr/api/geogw/services/556c6063330f1fcd48338644/feature-types/asp:RPG_2010_S_53/download?format=SHP&projection=Lambert93|RPG_2010_53.zip
https://www.data.gouv.fr/storage/f/2014-02-12T20-35-52/RPG_2012_035.zip|RPG_2012_035.zip
https://www.data.gouv.fr/storage/f/2014-02-13T13-23-11/RPG_2012_050.zip|RPG_2012_050.zip
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="")
  zip_dl(les_url);
}

zip_dl <- function(les_url, base="") {
  for(i in 1:nrow(les_url) ) {
    url <- les_url$url[i]
    zip <-sub(".*/", "", url)
    url <- sprintf("%s%s", base, url)
    print(sprintf("zip_dl() url:%s %s %s", url, zip, les_url$libelle[i]))
    file <- sprintf("%s/%s", dl_dir, zip)
    if( ! file.exists(file) ) {
      print(sprintf("zip_dl() dl file:%s", file ))
#      download.file(url, file, quiet = FALSE, mode = "wb")
    }
    if( ! file.exists(file) ) {
      print(sprintf("zip_dl() absent : file:%s", file))
	  stop()
    }
    zip_dir  <-sub("\\..*$", "", zip)
    zip_dir <- sprintf("%s/%s", dl_dir, zip_dir)
    if( ! file.exists(zip_dir) ) {
      print(sprintf("zip_dl() absent : zip_dir:%s", zip_dir))
      unzip(file, overwrite = TRUE,junkpaths = TRUE, exdir = zip_dir, unzip = "internal", setTimes = TRUE)
    } else {
      print(sprintf("zip_dl() present : zip_dir:%s", zip_dir))
    }
    files <- list.files(zip_dir, pattern = "\\.shp$", full.names = TRUE, ignore.case = TRUE)
    if ( length(files) != 1) {
      stop("zip_dl() .shp")
      next;
    }
    dsn <- files[1]
    layers <- ogrListLayers(dsn)
    print(sprintf("zip_dl() layers:%s", layers))
#    print(sprintf("inpn() info:%s", OGRSpatialRef(dsn,layers)))
    sp <- readOGR(dsn,layers)
    print(summary(sp))
  }
}
rpg_legend <- function() {
  les_legendes <- read.table(text="Valeur;Libelle;RGB
0;PAS D'INFORMATION;#FFFFFF
1;BLE TENDRE;#FFFF90
2;MAIS GRAIN ET ENSILAGE;#00FF00
3;ORGE;#F0FF70
4;AUTRES CEREALES;#DAEB00
5;COLZA;#FFECB0
6;TOURNESOL;#FFFF00
7;AUTRES OLEAGINEUX;#FFC000
8;PROTEAGINEUX;#F0B400
9;PLANTES A FIBRES;#C09000
10;SEMENCES;#604800
11;GEL (SURFACES GELEES SANS PRODUCTION);#F0F0F0
12;GEL INDUSTRIEL;#B0B0B0
13;AUTRES GELS;#D0D0D0
14;RIZ;#90B5FF
15;LEGUMINEUSES A GRAINS;#FFA080
16;FOURRAGE;#9FC75D
17;ESTIVES LANDES;#B5E66B
18;PRAIRIES PERMANENTES;#C0FF60
19;PRAIRIES TEMPORAIRES;#E0FFB0
20;VERGERS;#FF0000
21;VIGNES;#E000E0
22;FRUITS A COQUE;#008000
23;OLIVIERS;#A2A200
24;AUTRES CULTURES INDUSTRIELLES;#008080
25;LEGUMES-FLEURS;#FFA0D0
26;CANNE A SUCRE;#0000FF
27;ARBORICULTURE;#00B058
28;DIVERS;#800080", header=TRUE, sep=";", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="", encoding="UTF-8", comment.char = "")
  summary(les_legendes)
  head(les_legendes)
  maxY <- nrow(les_legendes) * 15
  par(mar=c(0, 0, 2, 0)) # c(bottom, left, top, right)
  plot(c(100, 200), c(0, maxY), type= "n", axes=FALSE ,xlab ="", ylab = "")
  print(sprintf("rpg_legend() nb_row:%s", nrow(les_legendes)))
  title(main="Registre Parcellaire Graphique")
  for (i in 1:nrow(les_legendes)) {
    y <- maxY - 15*i
    rect(100, 10+y, 115, 20+y, , col = les_legendes[i, "RGB"], border = "blue")
#	print(sprintf("rpg_legend() i:%s %s",i, les_legendes[i, "RGB"]))
# alignement en bas à gauche
    text(120, 10+y, les_legendes[i, "Libelle"], adj = c(0,0))
  }
}
#
# pour le registre parcellaire graphique
# ======================================
rpg_legendes_lire <- function() {
  df <- read.table(text="Valeur;Libelle;RGB
0;PAS D'INFORMATION;#FFFFFF
1;BLE TENDRE;#FFFF90
2;MAIS GRAIN ET ENSILAGE;#00FF00
3;ORGE;#F0FF70
4;AUTRES CEREALES;#DAEB00
5;COLZA;#FFECB0
6;TOURNESOL;#FFFF00
7;AUTRES OLEAGINEUX;#FFC000
8;PROTEAGINEUX;#F0B400
9;PLANTES A FIBRES;#C09000
10;SEMENCES;#604800
11;GEL (SURFACES GELEES SANS PRODUCTION);#F0F0F0
12;GEL INDUSTRIEL;#B0B0B0
13;AUTRES GELS;#D0D0D0
14;RIZ;#90B5FF
15;LEGUMINEUSES A GRAINS;#FFA080
16;FOURRAGE;#9FC75D
17;ESTIVES LANDES;#B5E66B
18;PRAIRIES PERMANENTES;#C0FF60
19;PRAIRIES TEMPORAIRES;#E0FFB0
20;VERGERS;#FF0000
21;VIGNES;#E000E0
22;FRUITS A COQUE;#008000
23;OLIVIERS;#A2A200
24;AUTRES CULTURES INDUSTRIELLES;#008080
25;LEGUMES-FLEURS;#FFA0D0
26;CANNE A SUCRE;#0000FF
27;ARBORICULTURE;#00B058
28;DIVERS;#800080", header=TRUE, sep=";", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="", encoding="UTF-8", comment.char = "")
#  summary(les_legendes)
  return(invisible(df))
}
#
# le rpg 2014 du département 35
rpg2014_plot <- function() {
  dl_dir <<- sprintf("%s/telechargement/", varDir)
  baie <- sprintf("%s/rpg_ilot_2014.shp", dl_dir)
  rpg.spdf <- ogr_lire(baie)
  print(head(rpg.spdf))
  plot(rpg.spdf, lwd = 3, add=TRUE)
}
#
# le rpg 2012 des département 35 et 50
rpg_plot <- function() {
  print(sprintf("couches_rpg_plot()"))
  dl_dir <<- sprintf("%s/telechargement/", varDir)
  baie <- sprintf("%s/rpg_baie.shp", dl_dir)
  rpg.spdf <- ogr_lire(baie)
  rpg.spdf <- cropSP(rpg.spdf, crop.spdf)
  rpg.spdf <- rpg.spdf[grep("^035", rpg.spdf@data$NUM_ILOT), ]
  legendes.df <- couches_rpg_legendes()
  couleurs <- legendes.df[,'RGB']
  print(head(couleurs))
  rpg.spdf@data <- merge(x=rpg.spdf@data, y=legendes.df, by.x="CULT_MAJ", by.y="Valeur", all.x=TRUE, all.y=FALSE, sort=FALSE)
  inconnuDF <- subset(rpg.spdf@data, is.na(rpg.spdf@data$RGB))
  if ( length(inconnuDF$RGB) > 0 ) {
    print(sprintf("couches_rpg_plot() couleur invalide nb: %d", nrow(inconnuDF)))
    print(head(inconnuDF))
    stop("couches_rpg_plot()")
  }
  rpg.spdf@data$libelle <- rpg.spdf@data$CULT_MAJ
  rpg.spdf@data$RGB <- sprintf("%s80", rpg.spdf@data$RGB)
  print(head(rpg.spdf@data))
#  plot(rpg.spdf, lwd = 3, col = couleurs[rpg.spdf@data$CULT_MAJ], add=TRUE)
  plot(rpg.spdf, lwd = 3, col = rpg.spdf@data$RGB, border = NA, add=TRUE)
#  text(coordinates(rpg.spdf), labels=rpg.spdf@data$libelle, font = 2, cex= 1)
#  stop("***")
#  plot(rpg.spdf, lwd = 3, col = rpg.spdf@data$RGB, add=TRUE)
}
#
# ========================================================================================
#
# version ign
#
# http://professionnels.ign.fr/rpg
# http://professionnels.ign.fr/doc/DC_DL_RPG_2-0.pdf
#
#
# source("geo/scripts/rpg.R");rpg1_stat(nc)
rpg2012_stat <- function(nc) {
  library(tidyverse)
  if(  exists('rpg1_correspondance.df') ) {
    rpg1_correspondance.df <<- rpg2_correspondance_lire() %>%
      distinct(code_groupe_culture, libelle_groupe_culture) %>%
      glimpse()
  }
  df <- nc %>%
    mutate(surf_ilot=round(as.numeric(st_area(.))/10000, 2)) %>%
    st_drop_geometry() %>%
    group_by(cult_maj) %>%
    summarize(surf=sum(surf_ilot)) %>%
    left_join(rpg1_correspondance.df, by=c('cult_maj'='code_groupe_culture')) %>%
    group_by(cult_maj, libelle_groupe_culture) %>%
    summarize(surf=sum(surf)) %>%
    mutate(code_group=sprintf('%s', cult_maj)) %>%
    dplyr::select(code_group, libelle_groupe_culture, surf) %>%
    glimpse()
  return(invisible(df))
}
rpg2013_stat <- function(nc) {
  library(tidyverse)
  if(  exists('rpg1_correspondance.df') ) {
    rpg1_correspondance.df <<- rpg2_correspondance_lire() %>%
      distinct(code_groupe_culture, libelle_groupe_culture) %>%
      glimpse()
  }
  df <- nc %>%
    st_drop_geometry() %>%
    group_by(code_cultu) %>%
    summarize(surf=sum(as.numeric(surf_cultu))) %>%
    mutate(cult_maj=as.integer(code_cultu)) %>%
    left_join(rpg1_correspondance.df, by=c('cult_maj'='code_groupe_culture')) %>%
    group_by(cult_maj, libelle_groupe_culture) %>%
    summarize(surf=sum(surf)) %>%
    mutate(code_group=sprintf('%s', cult_maj)) %>%
    dplyr::select(code_group, libelle_groupe_culture, surf) %>%
    glimpse()
  return(invisible(df))
}
#
# stat sur rpg version 1 ou version 2
rpg_stat <- function(nc) {
  df <- nc %>%
    mutate(surface=as.numeric(st_area(.)/10000)) %>%
    st_drop_geometry() %>%
    mutate(CODE_GROUP=if("CODE_GROUP" %in% names(.)){CODE_GROUP}else{CODE_CULTU}) %>%
    filter(!is.na(as.integer(CODE_GROUP))) %>%
    mutate(CODE_GROUP=as.integer(CODE_GROUP)) %>%
    group_by(CODE_GROUP, annee) %>%
    summarize(surface=sum(surface)) %>%
    glimpse()
#  stop('===')
  return(invisible(df))
}
#
# source("geo/scripts/rpg.R");nc <- rpg1_lire_sf()
rpg1_lire_sf <- function(annee='2013') {
  library(sf)
  library(tidyverse)
  switch(annee,
    '2010'={shpDir <- sprintf('%s/RPG_1-0__SHP_LAMB93_R053-2010_2010-01-01/RPG/1_DONNEES_LIVRAISON_2010/RPG_1-0_SHP_LAMB93_R053-2010', varDir)},
    '2011'={shpDir <- sprintf('%s/RPG_1-0__SHP_LAMB93_R53-2011_2011-01-01/RPG/1_DONNEES_LIVRAISON_2011/RPG_1-0_SHP_LAMB93_R53-2011', varDir)},
    '2012'={shpDir <- sprintf('%s/RPG_1-0__SHP_LAMB93_R53-2012_2012-01-01/RPG/1_DONNEES_LIVRAISON_2012/RPG_1-0_SHP_LAMB93_R53-2012', varDir)},
    '2013'={shpDir <- sprintf('%s/RPG_1-0_SHP_LAMB93_FXX_2016-02-16/RPG/1_DONNEES_LIVRAISON_2013/RPG_1-0_SHP_LAMB93_R53-2013', varDir)},
    '2014'={shpDir <- sprintf('%s/RPG_1-0__SHP_LAMB93_FXX_2017-04-07/RPG/1_DONNEES_LIVRAISON_2014/RPG_1-0_SHP_LAMB93_R053-2014', varDir)},
    {carp('****annee: %s', annee): stop()}
  )
  dsn <- sprintf('%s/%s.shp', shpDir, 'ILOTS_ANONYMES')
  nc <- st_read(dsn) %>%
    glimpse()
  return(invisible(nc))
}
#
# source("geo/scripts/rpg.R");nc <- rpg1_lire_dbf()
rpg1_lire_dbf <- function(annee='2017') {
  library(rio)
  library(tidyverse)
  switch(annee,
    '2010'={shpDir <- sprintf('%s/RPG_1-0__SHP_LAMB93_R053-2010_2010-01-01/RPG/1_DONNEES_LIVRAISON_2010/RPG_1-0_SHP_LAMB93_R053-2010', varDir)},
    '2011'={shpDir <- sprintf('%s/RPG_1-0__SHP_LAMB93_R53-2011_2011-01-01/RPG/1_DONNEES_LIVRAISON_2011/RPG_1-0_SHP_LAMB93_R53-2011', varDir)},
    '2012'={shpDir <- sprintf('%s/RPG_1-0__SHP_LAMB93_R53-2012_2012-01-01/RPG/1_DONNEES_LIVRAISON_2012/RPG_1-0_SHP_LAMB93_R53-2012', varDir)},
    '2013'={shpDir <- sprintf('%s/RPG_1-0_SHP_LAMB93_FXX_2016-02-16/RPG/1_DONNEES_LIVRAISON_2013/RPG_1-0_SHP_LAMB93_R53-2013', varDir)},
    '2014'={shpDir <- sprintf('%s/RPG_1-0__SHP_LAMB93_FXX_2017-04-07/RPG/1_DONNEES_LIVRAISON_2014/RPG_1-0_SHP_LAMB93_R053-2014', varDir)},
    {carp('****annee: %s', annee): stop()}
  )
  dsn <- sprintf('%s/%s.dbf', shpDir, 'ILOTS_ANONYMES')
  df <- rio::import(dsn) %>%
    glimpse()
  return(invisible(df))
}
#
# source("geo/scripts/rpg.R");nc <- rpg2_lire_sf()
rpg2_lire_sf <- function(annee='2017') {
  library(sf)
  library(tidyverse)
  shpDir <- sprintf('%s/RPG_2-0__SHP_LAMB93_R53-%s_%s-01-01/RPG/1_DONNEES_LIVRAISON_%s/RPG_2-0_SHP_LAMB93_R53-%s', varDir, annee, annee, annee, annee)
  dsn <- sprintf('%s/%s.shp', shpDir, 'PARCELLES_GRAPHIQUES')
  nc <- st_read(dsn) %>%
    glimpse()
  return(invisible(nc))
}
# source("geo/scripts/rpg.R");df <- rpg2_lire_dbf(annee='2018')
rpg2_lire_dbf <- function(annee='2017') {
  library(rio)
  library(tidyverse)
  carp('annee: %s', annee)
  shpDir <- sprintf('%s/RPG_2-0__SHP_LAMB93_R53-%s_%s-01-01/RPG/1_DONNEES_LIVRAISON_%s/RPG_2-0_SHP_LAMB93_R53-%s', varDir, annee, annee, annee, annee)
  dsn <- sprintf('%s/%s.dbf', shpDir, 'PARCELLES_GRAPHIQUES')
  df <- rio::import(dsn) %>%
    glimpse()
  return(invisible(df))
}
# https://geoservices.ign.fr/sites/default/files/2021-07/DC_DL_RPG_2-0.pdf
#
# source("geo/scripts/rpg.R");df <- rpg2_correspondance_lire()
rpg2_correspondance_lire <- function() {
  library(tidyverse)
  library(rio)
  library(janitor)
  dsn <- sprintf('%s/%s.csv', rpgDir, 'Codification_cultures_principales')
#  df <- read.table(dsn,, header=TRUE, sep=";", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="", encoding="UTF-8", comment.char = "") %>%
#    glimpse()
  df <- rio::import(dsn) %>%
    clean_names()
  return(invisible(df))
}
#
# source("geo/scripts/rpg.R");df <- rpg1_correspondance_lire()
rpg1_correspondance_lire <- function() {
  library(tidyverse)
  library(rio)
  library(janitor)
  dsn <- sprintf('%s/%s.csv', rpgDir, 'rpg1_codification_groupes_cultures')
  df <- rio::import(dsn) %>%
    clean_names()
  return(invisible(df))
}
#
# source("geo/scripts/rpg.R");rpg1_stat(nc)
rpg1_stat <- function(nc) {
  library(tidyverse)
  library(janitor)
  carp()
  if( ! exists('rpg1_correspondance.df') ) {
    rpg1_correspondance.df <<- rpg1_correspondance_lire() %>%
      glimpse()
  }
  if ( "sf" %in% class(nc)) {
    carp('transformation en data frame')
    df1 <- st_drop_geometry(nc)
  } else {
    df1 <- nc
  }
  df <- df1 %>%
    mutate_if(is.factor, as.character) %>%
    clean_names() %>%
    mutate(surf_cultu=as.numeric(surf_cultu)) %>%
    glimpse() %>%
    group_by(annee, code_cultu) %>%
    summarize(surf=sum(surf_cultu)) %>%
    mutate(code_group=as.numeric(code_cultu)) %>%
    left_join(rpg1_correspondance.df, by=c('code_group'='code_groupe_culture')) %>%
    group_by(annee, code_group, libelle_groupe_culture) %>%
    summarize(surf=sum(surf)) %>%
    arrange(code_group, annee) %>%
    ungroup()
  df %>%
    spread(annee, surf, fill=0) %>%
    adorn_totals(c("row")) %>%
    glimpse()
  return(invisible(df))
}
#
# source("geo/scripts/rpg.R");rpg2_stat(nc)
rpg2_stat <- function(nc) {
  library(tidyverse)
  library(janitor)
  carp()
  if( ! exists('rpg1_correspondance.df') ) {
    rpg1_correspondance.df <<- rpg1_correspondance_lire() %>%
      glimpse()
  }
  if ( "sf" %in% class(nc)) {
    carp('transformation en data frame')
    df1 <- st_drop_geometry(nc)
  } else {
    df1 <- nc
  }
  df <- df1 %>%
    clean_names() %>%
    mutate_if(is.factor, as.character) %>%
    mutate(code_group=as.numeric(code_group)) %>%
    glimpse() %>%
    group_by(annee, code_group) %>%
    summarize(surf=sum(surf_parc)) %>%
    left_join(rpg1_correspondance.df, by=c('code_group'='code_groupe_culture')) %>%
    ungroup() %>%
    dplyr::select(code_group, libelle_groupe_culture, annee, surf) %>%
    arrange(code_group, annee)
  df %>%
    spread(annee, surf, fill=0) %>%
    adorn_totals(c("row")) %>%
    glimpse()
  return(invisible(df))
}
#
# source("geo/scripts/rpg.R");rpg2_rotation()
rpg2_rotation <- function() {
  library(sf)
  library(tidyverse)
  pg2015.df <- rpg2_lire_sf('2015') %>%
    st_drop_geometry()
  pg2016.df <- rpg2_lire_sf('2016') %>%
    st_drop_geometry()
  pg2017.df <- rpg2_lire_sf('2017') %>%
    st_drop_geometry()
  pg2018.df <- rpg2_lire_sf('2018') %>%
    st_drop_geometry()
}
#
# rotations culturales
#
# https://geo.data.gouv.fr/fr/datasets/37b69d67f356c12e1bb9ef4c66d9c5a2a6456f61
# fichier à accès restreint
#
# 2013
# calcul fait par la DDTM du Finistère
# pas le département ni le n° d'ilot
#
# 2012
# https://lists.openstreetmap.org/pipermail/talk-fr/2016-February/079992.html
#
#
# site 2024 geoservices
#
# source("geo/scripts/rpg.R");rpg_geopf_dl()
rpg_geopf_dl <- function() {
  library(rvest)
  url <- "https://geoservices.ign.fr/rpg"
  session <- rvest::session(url)
  liens <- session %>%
    html_nodes("a") %>%
    html_attr("href") %>%
    glimpse()
  liens.df <- tibble(lien = liens) %>%
    filter(grepl("_R53[_-].*7z", lien)) %>%
    glimpse()
  misc_print(liens.df)
}
