# <!-- coding: utf-8 -->
#
# quelques fonctions pour l'atlas baie du mont-saint-michel
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
# ===============================================================
#

mga  <- function() {
  source("geo/scripts/bmsm.R");
}
#
# les commandes permettant le lancement
Drive <- substr( getwd(),1,2)
baseDir <- sprintf("%s/web", Drive)
ddtm35Dir <- sprintf("%s/bvi35/CouchesDDTM35", Drive);
varDir <- sprintf("%s/bvi35/CouchesBMSM", Drive);
cfgDir <- sprintf("%s/web/geo/BMSM", Drive)
texDir <- sprintf("%s/web/geo/BMSM", Drive)
wmsDir <-  sprintf("%s/web.var/geoportail/wms/BMSM", Drive)
couchesDir <- sprintf("%s/web.var/geo/couches", Drive)

setwd(baseDir)
source("geo/scripts/mga.R");
source("geo/scripts/misc.R");
source("geo/scripts/misc_carto.R");
source("geo/scripts/misc_couches.R");
source("geo/scripts/misc_faune.R");
source("geo/scripts/misc_fonds.R");
source("geo/scripts/misc_gdal.R");
source("geo/scripts/mga_gdal.R");
source("geo/scripts/misc_geo.R");
source("geo/scripts/misc_grille.R");
source("geo/scripts/misc_mnhn.R");
source("geo/scripts/misc_osm.R");
source("geo/scripts/misc_rpg.R");
source("geo/scripts/misc_tex.R");
source("geo/scripts/bmsm_atlas.R");
source("geo/scripts/bmsm_baie.R");
source("geo/scripts/bmsm_base.R");
source("geo/scripts/bmsm_cadastre.R");
source("geo/scripts/bmsm_cartes.R");
source("geo/scripts/bmsm_couches.R");
source("geo/scripts/bmsm_donnees.R");
source("geo/scripts/bmsm_especes.R");
source("geo/scripts/bmsm_excel.R");
source("geo/scripts/bmsm_faune.R");
source("geo/scripts/bmsm_fonds.R");
source("geo/scripts/bmsm_ocs.R");
# source("geo/scripts/bmsm_misc.R");
source("geo/scripts/bmsm_reff.R");
source("geo/scripts/bmsm_tex.R");
source("geo/scripts/bmsm_zones.R");
#source("geo/scripts/couches_gdal.R");
#
# quelques initialisations
pas <- 1000
opar <- par(mar = c(0,0,0,0))
DEBUG <- FALSE
if ( interactive() ) {
  print(sprintf("bmsm.R"))
  graphics.off()
} else {
}

