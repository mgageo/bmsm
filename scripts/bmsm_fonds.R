# <!-- coding: utf-8 -->
#
# la partie fonds de carte
# ===============================================================
#
# http://forums.cirad.fr/logiciel-R/viewtopic.php?t=6471
#
# source("geo/scripts/bmsm.R");fonds_elodie_tiff()
fonds_elodie_png <- function() {
  carp()
# https://danieljhocking.wordpress.com/2013/03/12/high-resolution-figures-in-r/
  spdf <- couches_extent()
  w <- extent(spdf)[2] - extent(spdf)[1]
  h <- extent(spdf)[4] - extent(spdf)[3]
  png("elodie.png", height = h/5, width = w/5, units = 'px')
  fonds_elodie()
  dev.off()
# https://www.rdocumentation.org/packages/gdalUtils/versions/2.0.1.14/topics/gdal_translate
}
fonds_elodie_tiff <- function() {
  carp()
  gdal_lib()
  library(raster)
# https://www.rdocumentation.org/packages/gdalUtils/versions/2.0.1.14/topics/gdal_translate
  spdf <- couches_extent()
  par(mfrow=c(1,1), mar=c(0,0,0,0), oma=c(0,0,0,0))
  a_ullr <- sprintf("%.2f %.2f %.2f %.2f", par("usr")[1], par("usr")[4], par("usr")[2], par("usr")[3])
#  a_ullr <- sprintf("%.2f %.2f %.2f %.2f", extent(spdf)[1], extent(spdf)[4], extent(spdf)[2], extent(spdf)[3])
  carp('ullr: %s', a_ullr)
  gdal_translate("elodie.png", "elodie.tif", of="GTiff",co="TILED=YES",a_ullr=a_ullr,a_srs="EPSG:2154", verbose=TRUE)
#  gdalinfo('elodie.tif')
  img <- raster('elodie.tif')
  plotImg(img)
  a_ullr <- sprintf("%.2f %.2f %.2f %.2f", par("usr")[1], par("usr")[4], par("usr")[2], par("usr")[3])
  carp('ullr: %s', a_ullr)
  fonds_elodie_villes()
}
#
# source("geo/scripts/bmsm.R");fonds_elodie()
fonds_elodie <- function() {
  library(sf)
  library(tidyverse)
  carp()
  par(mfrow=c(1,1), mar=c(0,0,0,0), oma=c(0,0,0,0))
  spdf <- couches_extent()
  plot(spdf, border = NA, col = NA, bg = "white")
  dsn <- sprintf('%s/baie_lambert_%s.shp', varDir, pas)
  grille.spdf <- ogr_lire(dsn)
  plot(grille.spdf, add=TRUE)
  df <- read.table(text="dsn;couleur
bocages_BMSM.shp;#aacd65
boisement_BMSM.shp;#267300
cultures_hors_polders_BMSM.shp;#ffd380
falaises_boisees_BMSM.shp;#4dff4d
herbus.shp;#c8d79e
iles_ilots_rochers.shp;#a87001
#limite_maritime_de_la_baie.shp;#d1faf4
#limite_maritime_de_la_baie_avec_complement_mer.shp;#4dff4d
#limite_terrestre_de_la_baie.shp;#4dff4d
#limite_terrestre_de_la_baie_sans_mont.shp;#4dff4d
marais_BMSM.shp;#bdd2ff
mer_sans_herbus.shp;#d1faf4
#perimetre_atlas_baie.shp;#4dff4d
polders_BMSM.shp;#d2ffbe
tissu_habitat_rural.shp;#ff7f7e
bourgs_hameaux.shp;#ff7f7e
", header=TRUE, sep=";", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="", comment.char = "") %>%
    filter(!grepl('^#', dsn)) %>%
    glimpse()
  for(i in 1:nrow(df) ) {
#    break;
    nc <- fonds_elodie_lire_sf(df[i, 'dsn'])
#    carp('couleur:%s', df[i, 'couleur']);    stop("***")
    plot(st_geometry(nc), col = df[i, 'couleur'], border = NA, add=TRUE)
  }
  df <- read.table(text="dsn;couleur
#limite_maritime_de_la_baie.shp;#d1faf4
#limite_maritime_de_la_baie_avec_complement_mer.shp;#4dff4d
#limite_terrestre_de_la_baie.shp;#4dff4d
#limite_terrestre_de_la_baie_sans_mont.shp;#4dff4d
perimetre_atlas_baie.shp;3
#trait_de_cote.shp;2
", header=TRUE, sep=";", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="", comment.char = "") %>%
    filter(!grepl('^#', dsn)) %>%
    glimpse()
  for(i in 1:nrow(df) ) {
    nc <- fonds_elodie_lire_sf(df[i, 'dsn'])
#    carp('couleur:%s', df[i, 'couleur']);    stop("***")
    plot(st_geometry(nc), col = NA, border = '#000000', lwd=df[i, 'couleur'], add=TRUE)
  }
  fonds_elodie_villes()
}
# source("geo/scripts/bmsm.R");fonds_elodie_villes()
fonds_elodie_villes_v1 <- function() {
  nc <- fonds_elodie_lire_sf('bourgs_hameaux.shp') %>%
    filter(! is.na(gdecom)) %>%
    glimpse()
  text(st_coordinates(st_centroid(nc)), labels=nc$commune, font = 2, cex= 1)
}
fonds_elodie_villes <- function() {
  df <- read.table(text="ville;X;Y
Avranches;378799;6851094
Granville;367123;6868893
Pontorson;366615;6836413
Cherrueix;357102;6843847
Dol-de-Bretagne;350626;6836123
Cancale;339123;6852225
", header=TRUE, sep=";", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="", comment.char = "") %>%
    filter(!grepl('^#', ville)) %>%
    glimpse()
  nc <- st_as_sf(df, coords = c("X", "Y"), crs = 2154)
  text(st_coordinates(nc), label=nc$ville, font = 2, cex= 1)
}
fonds_elodie_lire_sf <- function(dsn="marais_BMSM_Dissolve.shp") {
  carp()
  dsn <- sprintf('%s/SIG_BMSM_2016/%s', varDir, dsn)
  nc <- st_read(dsn, stringsAsFactors=FALSE) %>%
    glimpse()
  return(invisible(nc))
}
fonds_znieff <- function() {
  print(sprintf("fonds_znieff()"))
  par(mfrow=c(1,1), mar=c(0,0,1,0), oma=c(0,0,0,0))
  spdf <- couches_extent()
  plot(spdf, border = NA, col = NA, bg = "white")
  couches_route500_plot()
  spdf <- znieff_lire()
  plot(spdf, col = spdf@data$couleur, add=TRUE)
  dsn <- sprintf('%s/baie_lambert_%s.shp', varDir, pas)
  grille.spdf <- ogr_lire(dsn)
  plot(grille.spdf, add=TRUE)
}
fonds_zps <- function() {
  print(sprintf("fonds_zps()"))
  par(mfrow=c(1,1), mar=c(0,0,1,0), oma=c(0,0,0,0))
  spdf <- couches_extent()
  plot(spdf, border = NA, col = NA, bg = "white")
#  couches_route500_plot()
  spdf <- couches_zps_lire()
  plot(spdf, col = spdf@data$couleur, add=TRUE)
  dsn <- sprintf('%s/baie_lambert_%s.shp', varDir, pas)
  grille.spdf <- ogr_lire(dsn)
  plot(grille.spdf, add=TRUE)
}
fonds_communes <- function() {
#  par(mfrow=c(1,1), mar=c(0,0,0,0), oma=c(0,0,0,0))
  spdf <- couches_extent()
  plot(spdf, border = NA, col = "#BFD5EA", bg = "white")
  dl_dir <<- sprintf("%s/telechargement", varDir)
  dsn <- sprintf("%s/GEOFLA_COMMUNE.shp", dl_dir)
  spdf <- ogr_lire(dsn)
#  print(head(spdf@data,30))
  plot(spdf, col = "#dfbf9f40", add=TRUE)
#  print(head(spdf@data))
}
fonds_communes_sol <- function() {
  fonds_communes()
  dsn <- sprintf("%s/ZONE_OCCUPATION_SOL.shp", dl_dir)
  spdf <- ogr_lire(dsn)
  spdf@data$couleur <- "grey"
#  spdf@data[grep("Eau", spdf@data$NATURE,ignore.case=TRUE), "couleur"] <- "blue"
  s <- spdf[grep("B.*ti", spdf@data$NATURE,ignore.case=TRUE),]
  plot(s, col = "#ff6666", border = NA, add=TRUE)
  s <- spdf[grep("For.*t", spdf@data$NATURE,ignore.case=TRUE),]
  plot(s, col = " #4dff4d", border = NA, add=TRUE)
#  print(head(spdf@data))
}
# l'emprise à partir des données
fonds_emprise_lire_sf <- function() {
  carp()
  library(sf)
  dsn <- sprintf('%s/emprise.shp', varDir)
  nc <- st_read(dsn)
  nc <- st_transform(nc, "+init=epsg:2154")
  return(invisible(nc))
}
#
# les secteurs pour Jean-François Lebas
fonds_ens_lire <- function(secteur='Marais Mont-Dol sud') {
  dsn <- sprintf('%s/ens/%s.kml', varDir, secteur)
  carp("dsn: %s", dsn)
  spdf <- ogr_lire(dsn)
  return(invisible(spdf))
}
fonds_grille_lire <- function() {
  dsn <- sprintf('%s/baie_lambert_%s.json', varDir, pas)
  carp("dsn: %s", dsn)
  spdf <- ogr_lire(dsn)
  return(invisible(spdf))
}
# le perimètre utilisé par Matthieu pour l'atlas
fonds_grille_lire_sf <- function() {
  carp()
  library(sf)
  dsn <- sprintf('%s/baie_lambert_%s.geojson', varDir, pas)
  nc <- st_read(dsn, stringsAsFactors=FALSE, crs=2154)
#  nc <- st_transform(nc, "+init=epsg:2154")
  return(invisible(nc))
}
fonds_ign_cours_eau_lire <- function() {
  dsn <- sprintf('%s/ign/BDTOPO_BDD_WLD_WGS84G_troncon_cours_eau.json', varDir)
  carp("dsn: %s", dsn)
  spdf <- ogr_lire(dsn)
  return(invisible(spdf))
}
fonds_ign_cours_eau_lire_sf <- function() {
  if(exists('ign_cours_eau.sf')) {
    return(invisible(ign_cours_eau.sf))
  }
  dsn <- sprintf('%s/ign/BDTOPO_BDD_WLD_WGS84G_troncon_cours_eau.json', varDir)
  carp("dsn: %s", dsn)
  nc <- st_read(dsn)
  ign_cours_eau.sf <<- nc
  return(invisible(nc))
}
# source("geo/scripts/bmsm.R");nc <- fonds_ddtm35_cours_eau_lire_sf()
fonds_ddtm35_cours_eau_lire_sf <- function() {
  dsn <- sprintf('%s/l_carto_cours_eau_l_035.shp', ddtm35Dir)
  carp("dsn: %s", dsn)
  nc <- st_read(dsn)
  return(invisible(nc))
}
# le perimètre utilisé par Matthieu pour l'atlas
fonds_perimetre_lire_sf <- function() {
  carp()
  library(sf)
  dsn <- sprintf('%s/SIG_BMSM_2016/limites_baies/perimetre_atlas_baie.shp', varDir)
  nc <- st_read(dsn)
  return(invisible(nc))
}
#
# un raster en image de fonds
fonds_raster <- function(spdf) {
  require(raster)
  emprise <- as.vector (bbox(spdf))
  emprise <- englobante(emprise, marge=100, ratio=0, grille=2)
  xmin <- emprise[1]
  ymin <- emprise[2]
  xmax <- emprise[3]
  ymax <- emprise[4]
#  print(sprintf("fonds_raster(=ullr: %.0f %.0f %.0f %.0f lh: %.0fx%.0f", xmin, ymax, xmax, ymin,xmax-xmin,ymax-ymin))
  e <- extent(xmin, xmax, ymin, ymax)
  img <- crop(imgPVA, e)
  w <- img@ncols
  h <- img@nrows
  maxW <- 2048
  if ( maxW > 0 ) {
    if ( w > maxW ) {
      print(sprintf("fonds_raster() >%s w:%.0f h:%.0f", maxW, w, h))
      ratio <- w /maxW
      w <- ceiling(w / ratio)
      h <- ceiling(h / ratio)
    }
  }
  if ( interactive() ) {
    dev.new( width = w, height = h, units = "px", pointsize = 12)
  }
  plotRGB(img,axes=FALSE,add=FALSE, maxpixels=10000000)
}