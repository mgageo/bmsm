# <!-- coding: utf-8 -->
#
# quelques fonction pour l'atlas baie du mont-saint-michel
# auteur: Marc Gauthier
#
# http://gis.stackexchange.com/questions/32732/proper-way-to-rbind-spatialpolygonsdataframes-with-identical-polygon-ids
#
# source("geo/scripts/bmsm.R");zones_digue_especes('Polders')
zones_digue_especes <- function(partie="Polders") {
  carp()
  library(sqldf)
  library(ggplot2)
  zone.spdf <- couches_digue_lire(partie)
  donnees.spdf@data$nom <- over(donnees.spdf, zone.spdf)$nom
  df <- subset(donnees.spdf@data, ! is.na(donnees.spdf@data$nom))
#  print(head(df[, c("TAXO_VERNA")], 50))
  taxo.df <- sqldf("SELECT TAXO_LATIN, TAXO_VERNA, COUNT(*) as nb FROM df GROUP BY TAXO_LATIN, TAXO_VERNA;")
  taxo.df <- especes_camel(taxo.df)
  p <- ggplot(data=taxo.df, aes(x=espece, y=nb)) +
    coord_flip() +
    scale_x_discrete("") +
    geom_bar(stat="identity", position=position_dodge(), colour="black")
  print(p)
  return(p)
}
# source("geo/scripts/bmsm.R");zones_ens_carte(secteur='Marais les Grandes Patures')
zones_ens_carte <- function(secteur='Marais Mont-Dol sud', propriete='mbe3') {
  library(raster)
  carp()
  cartes_mailles_lire()
  spdf <- fonds_ens_lire(secteur=secteur)
  emprise <- as.vector (bbox(spdf))
  emprise <- englobante(emprise, marge=100, ratio=0)
  xmin <- emprise[1]
  ymin <- emprise[2]
  xmax <- emprise[3]
  ymax <- emprise[4]
  e <- extent(xmin, xmax, ymin, ymax)
  mailleImg <- crop(imgPVA, e)
  plotRGB(mailleImg,axes=FALSE,add=FALSE, maxpixels=10000000)
  plot(spdf, lwd = 3, add=TRUE)
  d.spdf <- donnees_mbe_lire('mbe4', force=FALSE)
#  glimpse(d.spdf@data)
  eau.spdf <- fonds_ign_cours_eau_lire()
  plot(eau.spdf, col = "blue", lwd=0.4, add=TRUE)
  nc <- fonds_ddtm35_cours_eau_lire_sf()
  plot(st_geometry(nc), col = "purple", lwd=0.6, add=TRUE)
#  plot(d.spdf, add=TRUE, cex=1)
  text(coordinates(d.spdf), labels=d.spdf@data$txt, cex=1, font=2)
  legend('topleft', legend=especes, cex=0.7, bty="n")
}
# source("geo/scripts/bmsm.R");zones_pumbo()
zones_pumbo <- function() {
  library(raster)
  carp()
  spdf <- donnees_lire()
  emprise <- as.vector (bbox(spdf))
  emprise <- englobante(emprise, marge=100, ratio=0)
  xmin <- emprise[1]
  ymin <- emprise[2]
  xmax <- emprise[3]
  ymax <- emprise[4]
  e <- extent(xmin, xmax, ymin, ymax)
  mailleImg <- crop(imgPVA, e)
  plotRGB(mailleImg,axes=FALSE,add=FALSE, maxpixels=10000000)
  plot(spdf, lwd = 3, add=TRUE)
}
#
# le filtrage pour Matthieu
# ==============================
#
# source("geo/scripts/bmsm.R");zones_mbe_jour()
zones_mbe_jour <- function(force=FALSE) {
#  for(couche in c('satellite', 'carte')) {
  for(couche in c('carte')) {
    for(propriete in c('mbe3', 'mbe4')) {
      zones_mbe_cartes(propriete=propriete, couche=couche)
    }
  }
}
# lecture d'un fichier avec les statuts
zones_mbe_lire <- function(force=FALSE) {
  if ( exists("zones_mbe.df") & force==FALSE) {
    return(zones_mbe.df)
  }
  library(rio)
  library(tidyverse)
  dsn <- sprintf("%s/zones_mbe.xlsx", cfgDir)
  carp('dsn: %s', dsn)
  df <- import(dsn)
  zones_mbe.df <<- df
  return(invisible(df))
}
# source("geo/scripts/bmsm.R");zones_mbe_cartes()
zones_mbe_cartes <- function(type="couples", propriete="mbe3", couche='carte') {
  carp()
  library(sf)
  communes.sf <<- fonds_commune_lire_sf() %>%
    glimpse() %>%
    dplyr::select(NOM_COM, INSEE_COM)
  df <- zones_mbe_lire(TRUE)
#
# la liste d'espèces à sélectionner
  especes_mbe.df <- especes_mbe_lire(force=TRUE) %>%
    glimpse()
  especes <<- especes_mbe.df[especes_mbe.df[, propriete] == 'oui', c('espece') ] %>%
    glimpse()
  donnees_lire_sf(FALSE)
  nc <<- donnees.sf %>%
    filter(espece %in% especes) %>%
    glimpse()
#
# pour la légende
  couleurs <- rep(c("black", "red", "green", "blue"), 10)
  pchs <- rep(c(21:25), 10)
  legende.df <<- tibble::enframe(unique(nc$espece), name=NULL) %>%
    rename(espece=value) %>%
    arrange(espece) %>%
    mutate(id=1:nrow(.)) %>%
    mutate(pch=pchs[id]) %>%
    mutate(col=couleurs[id]) %>%
    glimpse()
  donnees_mbe.sf <<- nc %>%
    left_join(legende.df)
  nc <- atlas_especes_mbe_lire(type=type, propriete=propriete)
  for(i in 1:nrow(df)) {
    zones_mbe_carte(type=type, propriete=propriete, nc, df, i, couche=couche)
#    break
  }
}
zones_mbe_carte <- function(type="couples", propriete="mbe4", nc, df, i, couche='satellite') {
  carp()
  library(sf)
  zps.sf <- couches_zps_lire_sf()
  zone.sf <- nc %>%
    filter(EN == df[i, 'maille1'] | EN == df[i, 'maille2']) %>%
    glimpse()
  bbox.sf <- st_bbox(zone.sf)
  mailles.sf <- st_crop(nc, bbox.sf) %>%
    glimpse()
#  fonds_ogc_ecrire(mailles.sf, df[i, 'zone'], force=TRUE, couches=c('carte'), size_max=2048)
  fonds_ogc_lire(df[i, 'zone'], force=TRUE)
#  plotImg(img_carte)
  plotImg(imgs[[couche]])
#  plot(st_geometry(mailles.sf), col=mailles.sf$couleur, border = "black", lwd=0.4, add=TRUE)
  plot(st_geometry(mailles.sf), col='transparent', border = "black", lwd=0.4, add=TRUE)
  plot(st_geometry(zps.sf), col='transparent', border = "darkgreen", lwd=4, add=TRUE)
  plot(st_geometry(communes.sf), col='transparent', border = "orange", lwd=2, add=TRUE)
  plot(st_geometry(donnees_mbe.sf), pch=donnees_mbe.sf$pch, col=donnees_mbe.sf$col, bg=donnees_mbe.sf$col, add=TRUE)
  legend('topleft', legend=legende.df$espece,
    cex=1,
    bg = 'white',
#    bty="n",
    pch=legende.df$pch,
    col = legende.df$col,
    pt.bg = legende.df$col
  )
#  scalebar(1000, type='bar', divs=2)
  geo_echelle()
  geo_copyright('@IGN Scan25 2017')
#  text(st_coordinates(st_centroid(mailles.sf)), labels = mailles.sf$nb, cex=2, col='black')
  dev2pdf(suffixe=sprintf('%s_%s_%s_%s', type, propriete, df[i, 'zone'], couche))
#  dev2png(suffixe=sprintf('%s_%s_%s_%s', type, propriete, df[i, 'zone'], couche), w=ncols, h=nrows)
}


# lecture du fichier avec la définition des labels
# source("geo/scripts/bmsm.R");zones_mbe_labels_lire()
zones_mbe_labels_lire <- function(force=FALSE) {
  if ( exists("zones_mbe_labels.df") & force==FALSE) {
    return(zones_mbe_labels.df)
  }
  library(rio)
  library(tidyverse)
  dsn <- sprintf("%s/zones_mbe.xlsx", cfgDir)
  carp('dsn: %s', dsn)
  df <- import(dsn, which='labels')
  zones_mbe_labels.df <<- df %>%
    glimpse()
  return(invisible(df))
}
# lecture du fichier avec la définition des zones
# source("geo/scripts/bmsm.R");zones_mbe_rectangles_lire()
zones_mbe_rectangles_lire <- function(force=FALSE) {
  if ( exists("zones_mbe_rectangles.df") & force==FALSE) {
    return(zones_mbe_rectangles.df)
  }
  library(rio)
  library(tidyverse)
  dsn <- sprintf("%s/zones_mbe.xlsx", cfgDir)
  carp('dsn: %s', dsn)
  df <- import(dsn, which='rectangles')
  zones_mbe_rectangles.df <<- df %>%
    glimpse()
  return(invisible(df))
}
# source("geo/scripts/bmsm.R");zones_mbe_rectangles_ogc()
zones_mbe_rectangles_ogc <- function(force=FALSE) {
  carp()
  library(rio)
  library(tidyverse)
  library(sf)
  donnees_lire_sf(FALSE)
  df <- zones_mbe_rectangles_lire(FALSE) %>%
    mutate(p1=sprintf('LINESTRING(%f %f, %f %f)', ulx, uly, lrx, lry)) %>%
    glimpse()
#  stop('******')
  nc <- st_as_sf(df, geometry=st_as_sfc(df$p1), crs = 2154, remove=FALSE) %>%
    glimpse()
  zone <- sprintf('%s', as.data.frame(nc)[1, 'zone'])
  carp('zone: %s', zone)
  couche <- 'satellite'
  couche <- 'gb_photo'
#  couche <- 'carte'
  fonds_ogc_ecrire(nc[1], nom=zone, marge=0, size_max=2048, couches=c('gp_em40', 'ddtm35_cours_eau.json', 'carte', 'gb_photo', 'bdtopo_hydro.json'), force=FALSE)
#  stop('***')
  fonds_ogc_lire(zone, force=TRUE)
  plotImg(imgs[[couche]], maxpixels=5000000)
  glimpse(donnees.sf)
#  plot(st_geometry(donnees.sf), col='darkred', pch=19, add=TRUE)
#  text(st_coordinates(donnees.sf), labels=donnees.sf$TAXO_VERNA, font = 2, cex= 1)
  nc <- imgs[['bdtopo_hydro']] %>%
   mutate(longueur= as.integer(st_length(.))) %>%
   glimpse()
  nc %>%
    summarize(nb=n(), longueur=sum(longueur)) %>%
    glimpse()
  plot(st_geometry(nc), col='#35AFC9', lwd=2, add=TRUE)
  nc <- imgs[['ddtm35_cours_eau']] %>%
   mutate(longueur= as.integer(st_length(.))) %>%
   glimpse()
  nc %>%
    summarize(nb=n(), longueur=sum(longueur)) %>%
    glimpse()
  plot(st_geometry(nc), col='blue', lwd=2, add=TRUE)
  geo_echelle(pos = "bottomright")
  zones_mbe_labels()
  return()
  dev2pdf(sprintf('%s/%s_%s.pdf', texDir, zone, couche))
  dev2png(sprintf('%s/%s_%s.png', texDir, zone, couche))
}
#
# carte d'état-major
# source("geo/scripts/bmsm.R");zones_mbe_rectangles_em40()
zones_mbe_rectangles_em40 <- function(force=TRUE) {
  carp()
  library(rio)
  library(tidyverse)
  library(sf)
  df <- zones_mbe_rectangles_lire(TRUE) %>%
    mutate(p1=sprintf('LINESTRING(%f %f, %f %f)', ulx, uly, lrx, lry)) %>%
    glimpse()
#  stop('******')
  nc <- st_as_sf(df, geometry=st_as_sfc(df$p1), crs = 2154, remove=FALSE) %>%
    glimpse()
  i <- 1
  zone <- sprintf('%s', as.data.frame(nc)[i, 'zone'])
  carp('zone: %s', zone)
  couche <- 'gp_em40'
  fonds_ogc_ecrire(nc[i], nom=zone, marge=0, size_max=2048, couches=c('gp_em40'), force=TRUE)
#  stop('***')
  imgs <- fonds_ogc_lire(zone, couches=c('gp_em40'), force=TRUE)
  glimpse(imgs)
  plotImg(imgs[[couche]], maxpixels=5000000)
  zps.sf <- couches_zps_lire_sf()
  plot(st_geometry(zps.sf), col='transparent', border = "black", lwd=6, add=TRUE)
  geo_echelle(pos = "bottomright")
#  zones_mbe_labels()
  return()
}
#
# ajout de labels
# - possibilité d'un cadre sous le texte
# source("geo/scripts/bmsm.R");zones_mbe_labels()
zones_mbe_labels <- function(force=FALSE, cadre=FALSE, rect.col='white', rect.border=NA, shadow=TRUE) {
  carp()
  library(tidyverse)
  library(sf)
  df <- zones_mbe_labels_lire(TRUE) %>%
    glimpse()
#  stop('******')
  nc <- st_as_sf(df, coords = c("x", "y"), crs = 2154, remove=FALSE) %>%
    glimpse()
  cc <- st_coordinates(nc)
  labels <- nc[['label']]
  cexs <- nc[['cex']]
#  plot(st_geometry(nc))
#
# le cadre
# label par label, strwidth ne supporte pas les vecteurs
  if (cadre == TRUE) {

    for(i in 1:length(labels)){
      cex <- cexs[i]
# léger agrandissement du cadre
      w <- strwidth(labels[i], font=2, cex=cex) * 1.1
      h <- strheight(labels[i], font=2, cex=cex) * 1.2
      tx <- cc[i, 1]
      ty <- cc[i, 2]
      rect(tx-0.5*w, ty-0.5*h, tx+0.5*w, ty+0.5*h, border = rect.border, col=rect.col)
#    legend(tx, ty, labels[i], cex=cex, col='white')
    }
  }
#  text(st_coordinates(nc), labels=nc$label, font = 2, cex= nc$cex)
  if(shadow == TRUE) {
    for(i in 1:length(labels)){
      carto_shadowtext(cc[i,1], cc[i,2], label = labels[i], cex=cexs[i], font=2)
    }
  }
}
#
# le marais de Dol
# source("geo/scripts/bmsm.R");zones_digues_et_marais()
zones_digues_et_marais <- function() {
  carp()
  library(tidyverse)
  library(sf)
  eau.sf <- fonds_ign_cours_eau_lire_sf()
  nc <- couches_digues_et_marais()
  sfc <- st_intersection(eau.sf, nc) %>%
    mutate(longueur= as.integer(st_length(.))) %>%
    st_drop_geometry() %>%
    summarize(longueur=sum(longueur)) %>%
    glimpse()
  nc <- fonds_grille_lire_sf() %>%
    st_transform(2154)
  nc %>%
    mutate(surface=as.numeric(round(st_area(.), 0))) %>%
    st_drop_geometry() %>%
    summarize(surface=sum(surface)) %>%
    glimpse()
  nc <- nc <- fonds_elodie_lire_sf('limite_terrestre_de_la_baie_sans_mont.shp')
  plot(st_geometry(nc))
  df <- nc %>%
    mutate(surface=as.numeric(round(st_area(.), 0))) %>%
    st_drop_geometry() %>%
    summarize(surface=sum(surface)) %>%
    glimpse()
}
