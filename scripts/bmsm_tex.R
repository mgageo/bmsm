# <!-- coding: utf-8 -->
#
# les traitements sur les données
#
#
# http://viktoriawagner.weebly.com/uploads/1/4/5/2/14527152/r-big-vegetation-data---iavs-23-nov-2015.pdf
# http://rstudio-pubs-static.s3.amazonaws.com/7993_6b081819ba184047802a508a7f3187cb.html

allcoordinates = function(x) {
  ret = NULL
  polys = x@polygons
  for(i in 1:length(polys)) {
    pp = polys[[i]]@Polygons
    ret = rbind(ret, coordinates(pp[[1]]))
  }
  ret
}
test2 <- function() {
  dsn <- sprintf("%s/grille_lambert_1000.shp", varDir)
  spdf <- ogr_lire(dsn)
  spdf@data$coord <- allcoordinates(spdf)
  print(head(spdf@data))
}

#
# la partie pour tex
tex <- function() {
  les_tex <- read.table(text="fonction|mode|args
#donnees||
#emprise_grille||
#baie_grille||
#donnees_maille_choro||
#atlas_especes||
#donnees_maille_nb|ggplot|
#donnees_maille||
#donnees_annee_carte|carte|
#donnees_annee_cartes|carte|
couches_digue_especes|ggplot|continental
couches_digue_carte|carte|continental
couches_digue_especes|ggplot|polders
couches_digue_carte|carte|polders
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="")
  for(i in 1:nrow(les_tex) ) {
    fonction <- les_tex$fonction[i]
    if ( grepl('^#', fonction) ) {
      next
    }
    tex_fonction(fonction, les_tex$mode[i], les_tex$args[i])
  }
}
tex_fonction <-function(e, mode="carte", args = "") {
  pdfFic <- sprintf("%s\\images\\%s.pdf", texDir, e)
  if ( grepl("carte", mode) ) {
    dev.new(width = ncols, height = nrows, units = "px")
  } else {
    pdf(pdfFic, width = 4, height = 4)
  }
#  dev.new()
  par(mar=c(0,0,0,0), oma=c(0,0,0,0))
  fonction <- sprintf("%s_tex", e)
  if( ! exists(fonction, mode = "function") ) {
    fonction <- e
  }
  a <- list()
  if ( args != "" ) {
    pdfFic <- sprintf("%s\\images\\%s_%s.pdf", texDir, e, args)
    a <- list(args)
  }
  p <- do.call(fonction, a)
  if ( grepl("carte", mode) ) {
    savePlot(filename=pdfFic,type=c("pdf"), device=dev.cur(), restoreConsole = TRUE)
  }
  if ( grepl("ggplot", mode) ) {
    save_plot(pdfFic, p)
  }
  print(sprintf("tex() e:%s pdfFic: %s", e, pdfFic))
}
